<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function index()
    {
        if ($this->session->userdata('username')) {
            redirect('admin');
        }
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $auth['data'] = $this->db->get("admin")->result();
        $this->load->view("auth", $auth);

        if ($username == $auth['data'][0]->username && $password == $auth['data'][0]->password) {
            $data = [
                'username' => $auth['data'][0]->username,
                'password' => $auth['data'][0]->password
            ];
            $this->session->set_userdata($data);
            redirect('admin');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Salah</div>');
        }
    }
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('password');

        redirect('auth');
    }
}
