<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        $this->load->view('admin');
    }
    public function keg_Mhs()
    {
        $mhs['data'] = $this->db->query("SELECT * FROM kegiatan WHERE kategori=2")->result();
        $this->load->view('keg_mhs', $mhs);
    }
    public function keg_Dsn()
    {
        $mhs['data'] = $this->db->query("SELECT * FROM kegiatan WHERE kategori=1")->result();
        $this->load->view('keg_dsn', $mhs);
    }
    public function tmbh_keg_M()
    {
        if ($this->form_validation->run() == false) {
            $this->load->view('tmbh_keg_mhs');
        } else {
            $data = [
                'nama_kegiatan' => $this->input->post('nama_kegiatan'),
                'kategori' => $this->input->post('kategori'),
                'waktu' => $this->input->post('waktu'),
                'tempat' => $this->input->post('tempat'),
                'terlibat' => $this->input->post('terlibat'),
                'documentasi' => $this->input->post('dokumentasi'),
                'keterangan' => $this->input->post('keterangan'),
            ];
            $ber = $this->db->insert('kegiatan', $data);
            var_dump($ber);
            $this->session->set_flashdata('message', 'Dibuat');
        }
    }
}
