<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title><?php $title ?></title>
</head>

<body>
    <div>
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1">Sistem Informasi Kegiatan PSTI</span>
            <a class="navbar-brand mb-0 h1" href="<?= base_url('auth/logout') ?>" style="float:right">Logout</a>
        </nav>
    </div>
    <div style="margin: 20px 20px 20px 20px;">
        <div class="row">
            <div class="col-3-sm">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="<?= base_url('admin') ?>">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('admin/keg_Mhs') ?>">Kegiatan Mahasiswa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('admin/keg_Dsn') ?>">Kegiatan Dosen</a>
                    </li>
                </ul>
            </div>
            <div class="col-9-sm">
                <div class="row">
                    <div class="col">
                        <button style="float:right"><a href="<?= base_url('admin/tmbh_Keg_M') ?>">Tambah Kegiatan</a></button>
                        <h1>Daftar Kegiatan Mahasiswa</h1>
                        <table class="table">
                            <tr>
                                <th>No</th>
                                <th>Nama Kegiatan</th>
                                <th>Kategori</th>
                                <th>Waktu</th>
                                <th>Tempat</th>
                                <th>Terlibat</th>
                                <th>Dokumentasi</th>
                                <th>Keterangan</th>
                            </tr>
                            <?php
                            $no = 1;
                            foreach ($data as $d) {
                                ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $d->nama_kegiatan ?></td>
                                    <td><?php if ($d->kategori == 1) {
                                                echo "Dosen";
                                            } else {
                                                echo "Mahasiswa";
                                            } ?></td>
                                    <td><?= $d->waktu ?></td>
                                    <td><?= $d->tempat ?></td>
                                    <td><?= $d->terlibat ?></td>
                                    <td><?= $d->documentasi ?></td>
                                    <td><?= $d->keterangan ?></td>
                                </tr>
                            <?php
                                $no++;
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</html>